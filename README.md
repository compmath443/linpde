# README #

This is LinPDE: a pipeline for solving convection–diffusion equation using a variety of physical models,
different initial parameters and boundary conditions. It also provides some tool for solution 
visualisations and numeric analisys.
Version: 0.0

### How do I get set up? ###

* Summary of set up: 
* Configuration:
* Dependencies:
* How to run tests:

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Made by 443 group of Department of Molecular and Chemical Physics (MIPT):

* Product Owner: Mikhail Petrov ()
* Team Leader: Aleksandr Severinov (aleksandr.severinov@phystech.edu)
* Research & Development: Almaz Khabibrahmanov (), 
  Vasily Vasilchenko (), Aleksey Tsarenko (), 
  Railj Akhmadeev (), Dmitry Popkov (),
  Aleksandr Glukhov ()
* Data Science & Visualisations: Ekaterina Sukhanova ()
* Questions & Answers: Vyacheslav Sivakov (), Lev Oganezyan ()
