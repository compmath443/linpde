import pandas as pd
from numpy import fft
import numpy as np
import matplotlib.pyplot as plt
import pylab
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib as mpl


def matrix_heatmap(x_axis, y_axis, matrix_function):

    vectorized_density = np.vectorize(matrix_function)
    z_axis = vectorized_density(x_axis, y_axis)

    plt.pcolormesh(x_axis, y_axis, z_axis)
    plt.show()
    return 0


def building_1d_dependence(data, axes):
    """Build a 1D dependence in given coordinates.

    Keyword arguments:
    data         -- a data frame of equation solutions
    axes         -- an array consisting of signatures of axes and title [x, y, title]
    """

    # converting data to array
    data = data.values
    x = []
    y = []
    for i in range(len(data)):
        x.append(data[i][0])
        y.append(data[i][1])

    figure_size = (16, 12)
    figure = plt.figure(figsize=figure_size)

    plt.xlabel(axes[0])
    plt.ylabel(axes[1])
    plt.title(axes[2])
    plt.grid(True)
    plt.plot(x, y)
    plt.show()


def building_2d_plot(data, coordinates_boundaries, axes):
    """Build a 2D plot(wavefield).

    Keyword arguments:
    data                   -- a three-dimensional array of equation solutions
    coordinates_boundaries -- [[x.min, x.max], [y.min, y.max]]
    axes                   -- an array consisting of signatures of axes and title [x, y, title]

    """

    xmin, xmax = coordinates_boundaries[0]
    ymin, ymax = coordinates_boundaries[1]

    fig = plt.figure(figsize=(14, 6))

    n = 10  # the number of signatures for color scale
    bounds = np.linspace(np.min(data), np.max(data), n)
    cmap = mpl.cm.get_cmap('gnuplot2_r', n)

    ax = fig.add_subplot(111)
    cs = ax.contourf(data, cmap=cmap, vmin=bounds[0], vmax=bounds[-1])
    cbar = fig.colorbar(cs, ax=ax, pad=0.01, ticks=bounds, extend='neither', orientation='vertical')

    ax.set_title(axes[2])

    # с осями чего-то не то
    plt.xlabel(axes[0])
    plt.ylabel(axes[1])
    x_label = np.linspace(xmin, xmax, 6)
    y_label = np.linspace(ymin, ymax, 6)

    ax.set_xticklabels(x_label, rotation=45)
    ax.set_yticklabels(y_label, rotation=45)
    ax.grid(True)

    plt.tight_layout()
    plt.show()


def building_3d_plot(array, boundaries, x_step, y_step, axes):
    """Build a 3D plot in given time node.

    Keyword arguments:
    array       -- a two-dimensional array of equation solutions [t[x[y]]]
    boundaries  -- boundaries of the area in format [[xmin, xmax], [ymin, ymax]]
    x_step          -- grid step for the variable x
    y_step          -- grid step for the variable y
    axes        -- an array consisting of signatures of axes and title [x, y, title]

    """
    '''
    comments from me:
        this function works if massive a satisfies some dependency
        sometimes it can't build a dependence, so i need to check it
    '''

    xmin, xmax = boundaries[0]
    ymin, ymax = boundaries[1]

    x = np.arange(xmin, xmax, x_step)
    y = np.arange(ymin, ymax, y_step)
    x, y = np.meshgrid(x, y)  # grid creation

    z = array

    fig = pylab.figure()
    ax = Axes3D(fig)
    ax.plot_surface(x, y, z, rstride=4, cstride=4, cmap='jet')
    plt.xlabel(axes[0])
    plt.ylabel(axes[1])
    plt.title(axes[2])

    pylab.show()


def fast_fourier_transformation(signal, step, boundaries, title):
    """Build Fourier Transform for the signal.

    Keyword arguments:
    signal      -- 1D array
    step        -- grid step for the variable t
    boundaries  -- boundaries of the area in format [time_min, time_max]
    """  

    time_min, time_max = boundaries

    # t coordinates
    time = np.arange(time_min, time_max + step, step)

    # number of data points
    n = len(time)

    # Fourier coefficients (divided by n)
    spectrum = fft.fftn(signal) / n

    # Natural frequencies
    frequencies = fft.fftfreq(n, step)

    # Shift zero freq to center
    spectrum = fft.fftshift(spectrum)  # ???
    frequencies = fft.fftshift(frequencies)

    f, ax = plt.subplots(3, 1, sharex=True)
    ax[0].set_title(r'$Fourier\:\:transform\:\:of\:$' + title)
    ax[0].plot(frequencies, np.real(spectrum))  # Plot Cosine terms
    ax[0].set_ylabel(r'$Re[F_k]$', size='x-large')
    ax[1].plot(frequencies, np.real(spectrum))  # Plot Sine terms
    ax[1].set_ylabel(r'$Im[F_k]$', size='x-large')
    ax[2].plot(frequencies, np.absolute(spectrum) ** 2)  # Plot spectral power
    ax[2].set_ylabel(r'$\vert F_k \vert ^2$', size='x-large')
    ax[2].set_xlabel(r'$\widetilde{\nu}, Hz$', size='x-large')
    plt.show()


def building_plot(type_of_equation, file):
    """The main function which build graphs

    Keyword arguments:
    type_of_equation -- type of equation: 'seismic' / 'acoustic'
    file             -- the name of file with solutions
    """
    # reading txt file as data series
    solutions = pd.read_csv(file, sep='\t')

    # parameters of the task
    time_step = (solutions['time'][len(solutions['time']) - 1] - solutions['time'][0]) / \
                (solutions['time'].nunique() - 1)

    # grid step for variables x and y
    x_step = (solutions['x'][len(solutions['x']) - 1] - solutions['x'][0]) / (solutions['x'].nunique() - 1)
    y_step = (solutions['y'][len(solutions['y']) - 1] - solutions['y'][0]) / (solutions['y'].nunique() - 1)
    amount_of_points_t = solutions['time'].nunique()
    amount_of_points_x = solutions['x'].nunique()
    amount_of_points_y = solutions['y'].nunique()

    # boundaries
    time_boundaries = [solutions['time'].min(), solutions['time'].max()]
    x_boundaries = [solutions['x'].min(), solutions['x'].max()]
    y_boundaries = [solutions['y'].min(), solutions['y'].max()]

    print('Please chose the type of the graph (1D, 2D, 3D, FFT, animation):')
    type_of_graph = input()

    if type_of_equation == 'seismic':

        if type_of_graph == '1D':

            data = solutions
            print('\nEnter x-variable (time, x, y):')
            x_variable = input()
            print('\nEnter y-variable (sigma_11, sigma_12, sigma_13, u1, u2):')
            y_variable = input()

            refinement = '('

            # надо сделать покрасивше
            for col in data.columns:
                if (col != x_variable) & (col != y_variable):
                    if col == 'time':
                        print('\nPlease fix the ' + col)
                        value = int(input())
                        data = data[data.time == value]
                        refinement += (' time = ' + str(value))
                        data = data.drop(['time'], axis='columns')
                    elif col == 'x':
                        print('\nPlease fix the ' + col)
                        value = int(input())
                        data = data[data.x == value]
                        refinement += (' x = ' + str(value))
                        data = data.drop(['x'], axis='columns')
                    elif col == 'y':
                        print('\nPlease fix the ' + col)
                        value = int(input())
                        data = data[data.y == value]
                        refinement += (' y = ' + str(value))
                        data = data.drop(['y'], axis='columns')
                    elif col == 'sigma_11':
                        data = data.drop(['sigma_11'], axis='columns')
                    elif col == 'sigma_12':
                        data = data.drop(['sigma_12'], axis='columns')
                    elif col == 'sigma_13':
                        data = data.drop(['sigma_13'], axis='columns')
                    elif col == 'u1':
                        data = data.drop(['u1'], axis='columns')
                    else:
                        data = data.drop(['u2'], axis='columns')
            refinement += ')'

            axes = [x_variable, y_variable, 'The dependence of ' + y_variable + ' from the ' + x_variable + refinement]
            building_1d_dependence(data, axes)

        elif type_of_graph == 'FFT':

            data = solutions

            x_variable = 'time'
            print('\nEnter y-variable (sigma_11, sigma_12, sigma_13, u1, u2):')
            y_variable = input()

            refinement = '('

            # надо сделать покрасивше
            for col in data.columns:
                if (col != x_variable) & (col != y_variable):
                    if col == 'x':
                        print('\nPlease fix the ' + col)
                        value = int(input())
                        data = data[data.x == value]
                        refinement += (' x = ' + str(value))
                        data = data.drop(['x'], axis='columns')
                    elif col == 'y':
                        print('\nPlease fix the ' + col)
                        value = int(input())
                        data = data[data.y == value]
                        refinement += (' y = ' + str(value))
                        data = data.drop(['y'], axis='columns')
                    elif col == 'sigma_11':
                        data = data.drop(['sigma_11'], axis='columns')
                    elif col == 'sigma_12':
                        data = data.drop(['sigma_12'], axis='columns')
                    elif col == 'sigma_13':
                        data = data.drop(['sigma_13'], axis='columns')
                    elif col == 'u1':
                        data = data.drop(['u1'], axis='columns')
                    else:
                        data = data.drop(['u2'], axis='columns')
            refinement += ')'

            data = data.drop(['time'], axis='columns')
            step = time_step
            boundaries = time_boundaries
            data = data.values
            values = []
            for i in range(len(data)):
                values.append(data[i][0])
            title = 'The dependence of ' + y_variable + ' from the ' + x_variable + refinement
            fast_fourier_transformation(data, step, boundaries, title)

        elif type_of_graph == '2D':

            data = solutions

            print('\nEnter x-axis (time, x, y):')
            x_axis = input()

            print('\nEnter y-axis (time, x, y):')
            y_axis = input()

            print('\nEnter a variable (sigma_11, sigma_12, sigma_13, u1, u2):')
            variable = input()

            if ((x_axis == 'time') & (y_axis == 'x')) or ((x_axis == 'x') & (y_axis == 'time')):
                type_of_fixed_parameter = 'y'
            elif ((x_axis == 'time') & (y_axis == 'y')) or ((x_axis == 'y') & (y_axis == 'time')):
                type_of_fixed_parameter = 'x'
            else:
                type_of_fixed_parameter = 'time'
            print('\nEnter the value of fixed ' + type_of_fixed_parameter + ' value:')
            value_of_fixed_parameter = float(input())

            refinement = '(' + type_of_fixed_parameter + ' ' + str(value_of_fixed_parameter) + ')'

            if type_of_fixed_parameter == 'time':
                data = data[data.time == value_of_fixed_parameter]
                data = data.drop(['time'], axis='columns')
            elif type_of_fixed_parameter == 'x':
                data = data[data.x == value_of_fixed_parameter]
                data = data.drop(['x'], axis='columns')
            elif type_of_fixed_parameter == 'y':
                data = data[data.y == value_of_fixed_parameter]
                data = data.drop(['y'], axis='columns')
            for col in data.columns:
                if (col != variable) & (col != type_of_fixed_parameter):
                    if col == 'sigma_11':
                        data = data.drop(['sigma_11'], axis='columns')
                    elif col == 'time':

                        data = data.drop(['time'], axis='columns')
                    elif col == 'x':
                        data = data.drop(['x'], axis='columns')
                    elif col == 'y':
                        data = data.drop(['y'], axis='columns')
                    elif col == 'sigma_12':
                        data = data.drop(['sigma_12'], axis='columns')
                    elif col == 'sigma_13':
                        data = data.drop(['sigma_13'], axis='columns')
                    elif col == 'u1':
                        data = data.drop(['u1'], axis='columns')
                    elif col == 'u2':
                        data = data.drop(['u2'], axis='columns')
            data = data.values
            if x_axis == 'time':
                par1 = amount_of_points_t
                bound1 = time_boundaries
            elif x_axis == 'x':
                par1 = amount_of_points_x
                bound1 = x_boundaries
            else:
                par1 = amount_of_points_y
                bound1 = y_boundaries
            if y_axis == 'time':
                par2 = amount_of_points_t
                bound2 = time_boundaries
            elif y_axis == 'x':
                par2 = amount_of_points_x
                bound2 = x_boundaries
            else:
                par2 = amount_of_points_y
                bound2 = y_boundaries
            data = data.reshape(par2, par1)
            building_2d_plot(data, [bound1, bound2],
                             [x_axis, y_axis, 'The dependence of ' + variable + ' from the ' + x_axis +
                              ' and ' + y_axis + refinement])
        elif type_of_graph == '3D':

            data = solutions

            print('\nEnter x-axis (time, x, y):')
            x_axis = input()

            print('\nEnter y-axis (time, x, y):')
            y_axis = input()

            print('\nEnter a variable (sigma_11, sigma_12, sigma_13, u1, u2):')
            variable = input()

            if ((x_axis == 'time') & (y_axis == 'x')) or ((x_axis == 'x') & (y_axis == 'time')):
                type_of_fixed_parameter = 'y'
            elif ((x_axis == 'time') & (y_axis == 'y')) or ((x_axis == 'y') & (y_axis == 'time')):
                type_of_fixed_parameter = 'x'
            else:
                type_of_fixed_parameter = 'time'
            print('\nEnter the value of fixed ' + type_of_fixed_parameter + ' value:')
            value_of_fixed_parameter = float(input())

            refinement = '(' + type_of_fixed_parameter + ' ' + str(value_of_fixed_parameter) + ')'

            if type_of_fixed_parameter == 'time':
                data = data[data.time == value_of_fixed_parameter]
                data = data.drop(['time'], axis='columns')
            elif type_of_fixed_parameter == 'x':
                data = data[data.x == value_of_fixed_parameter]
                data = data.drop(['x'], axis='columns')
            elif type_of_fixed_parameter == 'y':
                data = data[data.y == value_of_fixed_parameter]
                data = data.drop(['y'], axis='columns')
            for col in data.columns:
                if (col != variable) & (col != type_of_fixed_parameter):
                    if col == 'sigma_11':
                        data = data.drop(['sigma_11'], axis='columns')
                    elif col == 'time':

                        data = data.drop(['time'], axis='columns')
                    elif col == 'x':
                        data = data.drop(['x'], axis='columns')
                    elif col == 'y':
                        data = data.drop(['y'], axis='columns')
                    elif col == 'sigma_12':
                        data = data.drop(['sigma_12'], axis='columns')
                    elif col == 'sigma_13':
                        data = data.drop(['sigma_13'], axis='columns')
                    elif col == 'u1':
                        data = data.drop(['u1'], axis='columns')
                    elif col == 'u2':
                        data = data.drop(['u2'], axis='columns')
            data = data.values
            if x_axis == 'time':
                par1 = amount_of_points_t
                bound1 = time_boundaries
            elif x_axis == 'x':
                par1 = amount_of_points_x
                bound1 = x_boundaries
            else:
                par1 = amount_of_points_y
                bound1 = y_boundaries
            if y_axis == 'time':
                par2 = amount_of_points_t
                bound2 = time_boundaries
            elif y_axis == 'x':
                par2 = amount_of_points_x
                bound2 = x_boundaries
            else:
                par2 = amount_of_points_y
                bound2 = y_boundaries
            data = data.reshape(par2, par1)
            building_3d_plot(data, [bound1, bound2], x_step, y_step,
                             [x_axis, y_axis, 'The dependence of ' + variable + ' from the ' + x_axis +
                              ' and ' + y_axis + refinement])
    else:
        if type_of_graph == '1D':

            data = solutions
            print('\nPlease enter x-variable (time, x, y):')
            x_variable = input()
            print('\nPlease enter y-variable (p, u1, u2):')
            y_variable = input()

            refinement = '('

            # надо сделать покрасивше
            for col in data.columns:
                if (col != x_variable) & (col != y_variable):
                    if col == 'time':
                        print('\nPlease fix the ' + col)
                        value = int(input())
                        data = data[data.time == value]
                        refinement += (' time = ' + str(value))
                        data = data.drop(['time'], axis='columns')
                    elif col == 'x':
                        print('\nPlease fix the ' + col)
                        value = int(input())
                        data = data[data.x == value]
                        refinement += (' x = ' + str(value))
                        data = data.drop(['x'], axis='columns')
                    elif col == 'y':
                        print('\nPlease fix the ' + col)
                        value = int(input())
                        data = data[data.y == value]
                        refinement += (' y = ' + str(value))
                        data = data.drop(['y'], axis='columns')
                    elif col == 'p':
                        data = data.drop(['p'], axis='columns')
                    elif col == 'u1':
                        data = data.drop(['u1'], axis='columns')
                    else:
                        data = data.drop(['u2'], axis='columns')
            refinement += ')'
            axes = [x_variable, y_variable, 'The dependence of ' + y_variable + ' from the ' + x_variable + refinement]

            building_1d_dependence(data, axes)

        elif type_of_graph == 'FFT':

            data = solutions

            x_variable = 'time'
            print('\nPlease enter y-variable (p, u1, u2):')
            y_variable = input()

            refinement = '('

            # надо сделать покрасивше
            for col in data.columns:
                if (col != x_variable) & (col != y_variable):
                    if col == 'x':
                        print('\nPlease fix the ' + col)
                        value = int(input())
                        data = data[data.x == value]
                        refinement += (' x = ' + str(value))
                        data = data.drop(['x'], axis='columns')
                    elif col == 'y':
                        print('\nPlease fix the ' + col)
                        value = int(input())
                        data = data[data.y == value]
                        refinement += (' y = ' + str(value))
                        data = data.drop(['y'], axis='columns')
                    elif col == 'p':
                        data = data.drop(['p'], axis='columns')
                    elif col == 'u1':
                        data = data.drop(['u1'], axis='columns')
                    else:
                        data = data.drop(['u2'], axis='columns')
            refinement += ')'

            data = data.drop(['time'], axis='columns')

            step = time_step
            boundaries = time_boundaries

            data = data.values
            values = []
            for i in range(len(data)):
                values.append(data[i][0])

            title = 'The dependence of ' + y_variable + ' from the ' + x_variable + refinement

            fast_fourier_transformation(data, step, boundaries, title)

        elif type_of_graph == '2D':

            data = solutions

            print('\nEnter x-axis (time, x, y):')
            x_axis = input()

            print('\nEnter y-axis (time, x, y):')
            y_axis = input()

            print('\nEnter a variable (p, u1, u2):')
            variable = input()

            if ((x_axis == 'time') & (y_axis == 'x')) or ((x_axis == 'x') & (y_axis == 'time')):
                type_of_fixed_parameter = 'y'
            elif ((x_axis == 'time') & (y_axis == 'y')) or ((x_axis == 'y') & (y_axis == 'time')):
                type_of_fixed_parameter = 'x'
            else:
                type_of_fixed_parameter = 'time'
            print('\nEnter the value of fixed ' + type_of_fixed_parameter + ' value:')
            value_of_fixed_parameter = float(input())

            refinement = '(' + type_of_fixed_parameter + ' ' + str(value_of_fixed_parameter) + ')'

            if type_of_fixed_parameter == 'time':
                data = data[data.time == value_of_fixed_parameter]
                data = data.drop(['time'], axis='columns')
            elif type_of_fixed_parameter == 'x':
                data = data[data.x == value_of_fixed_parameter]
                data = data.drop(['x'], axis='columns')
            elif type_of_fixed_parameter == 'y':
                data = data[data.y == value_of_fixed_parameter]
                data = data.drop(['y'], axis='columns')
            for col in data.columns:
                if (col != variable) & (col != type_of_fixed_parameter):
                    if col == 'p':
                        data = data.drop(['p'], axis='columns')
                    elif col == 'time':
                        data = data.drop(['time'], axis='columns')
                    elif col == 'x':
                        data = data.drop(['x'], axis='columns')
                    elif col == 'y':
                        data = data.drop(['y'], axis='columns')
                    elif col == 'u1':
                        data = data.drop(['u1'], axis='columns')
                    elif col == 'u2':
                        data = data.drop(['u2'], axis='columns')
            data = data.values
            if x_axis == 'time':
                par1 = amount_of_points_t
                bound1 = time_boundaries
            elif x_axis == 'x':
                par1 = amount_of_points_x
                bound1 = x_boundaries
            else:
                par1 = amount_of_points_y
                bound1 = y_boundaries
            if y_axis == 'time':
                par2 = amount_of_points_t
                bound2 = time_boundaries
            elif y_axis == 'x':
                par2 = amount_of_points_x
                bound2 = x_boundaries
            else:
                par2 = amount_of_points_y
                bound2 = y_boundaries
            data = data.reshape(par2, par1)
            building_2d_plot(data, [bound1, bound2],
                             [x_axis, y_axis, 'The dependence of ' + variable + ' from the ' + x_axis +
                              ' and ' + y_axis + refinement])
        elif type_of_graph == '3D':

            data = solutions

            print('\nEnter x-axis (time, x, y):')
            x_axis = input()

            print('\nEnter y-axis (time, x, y):')
            y_axis = input()

            print('\nEnter a variable (p, u1, u2):')
            variable = input()

            if ((x_axis == 'time') & (y_axis == 'x')) or ((x_axis == 'x') & (y_axis == 'time')):
                type_of_fixed_parameter = 'y'
            elif ((x_axis == 'time') & (y_axis == 'y')) or ((x_axis == 'y') & (y_axis == 'time')):
                type_of_fixed_parameter = 'x'
            else:
                type_of_fixed_parameter = 'time'
            print('\nEnter the value of fixed ' + type_of_fixed_parameter + ' value:')
            value_of_fixed_parameter = float(input())

            refinement = '(' + type_of_fixed_parameter + ' ' + str(value_of_fixed_parameter) + ')'

            if type_of_fixed_parameter == 'time':
                data = data[data.time == value_of_fixed_parameter]
                data = data.drop(['time'], axis='columns')
            elif type_of_fixed_parameter == 'x':
                data = data[data.x == value_of_fixed_parameter]
                data = data.drop(['x'], axis='columns')
            elif type_of_fixed_parameter == 'y':
                data = data[data.y == value_of_fixed_parameter]
                data = data.drop(['y'], axis='columns')
            for col in data.columns:
                if (col != variable) & (col != type_of_fixed_parameter):
                    if col == 'p':
                        data = data.drop(['p'], axis='columns')
                    elif col == 'time':
                        data = data.drop(['time'], axis='columns')
                    elif col == 'x':
                        data = data.drop(['x'], axis='columns')
                    elif col == 'y':
                        data = data.drop(['y'], axis='columns')
                    elif col == 'u1':
                        data = data.drop(['u1'], axis='columns')
                    elif col == 'u2':
                        data = data.drop(['u2'], axis='columns')
            data = data.values
            if x_axis == 'time':
                par1 = amount_of_points_t
                bound1 = time_boundaries
            elif x_axis == 'x':
                par1 = amount_of_points_x
                bound1 = x_boundaries
            else:
                par1 = amount_of_points_y
                bound1 = y_boundaries
            if y_axis == 'time':
                par2 = amount_of_points_t
                bound2 = time_boundaries
            elif y_axis == 'x':
                par2 = amount_of_points_x
                bound2 = x_boundaries
            else:
                par2 = amount_of_points_y
                bound2 = y_boundaries
            data = data.reshape(par2, par1)
            building_3d_plot(data, [bound1, bound2], x_step, y_step,
                             [x_axis, y_axis, 'The dependence of ' + variable + ' from the ' + x_axis +
                              ' and ' + y_axis + refinement])
