from src.medium import *
from src.methods.mesh import *
from src.matrix import *
from src.boundary_conditions import *
from src.source import *


class Model:
    """ This class unites everything needed for solver.
    
    Attributes:
        _mesh (:obj: Mesh2D): mesh, which is discretization of 2D solid medium.
        _medium (:obj: Medium):
        _model_type (str): type of unknown vector u and equation itself.
            ["Seismic", "Acoustic"]
        _matrix_a (numpy matrix): 
        _matrix_b (numpy matrix):
        _boundary_conditions ():
        _initial ():
        _source ():
    """

    __slots__ = ["_medium", "_mesh", "_model_type", "_matrix_a", "_matrix_b", "_boundary_conditions", "_initial",
                 "_source"]

    def __init__(self, model_type, x_start, x_end, y_start, y_end, x_num_points, y_num_points,
                 density, lame_lambda, lame_mu, u_initial, b1, b2, f1, b3, b4, f2, b5, b6, f3, b7, b8, f4):

        self._mesh = Mesh2D(x_start, x_end, y_start, y_end, x_num_points, y_num_points)
        self._medium = Medium(density, lame_lambda, lame_mu)
        self._initial = u_initial
        self._boundary_conditions = BoundaryConditions(x_start, x_end, y_start, y_end,
                                                       b1, b2, f1, b3, b4, f2, b5, b6, f3, b7, b8, f4)
        self._model_type = model_type

        if self._model_type == "Seismic":
            self._matrix_a = Matrix(density, lame_lambda, lame_mu).matrix_a_seismic
            self._matrix_b = Matrix(density, lame_lambda, lame_mu).matrix_b_seismic
        elif self._model_type == "Acoustic":
            self._matrix_a = Matrix(density, lame_lambda, lame_mu).matrix_a_acoustic
            self._matrix_b = Matrix(density, lame_lambda, lame_mu).matrix_b_acoustic

        self._source = None

    def add_point_source(self, x, y, impulse):
        self._source = Source(x, y, impulse)

    def medium(self):
        return self._medium

    def mesh(self):
        return self._mesh

    def matrix_a(self):
        return self._matrix_a

    def matrix_b(self):
        return self._matrix_b

    def model_type(self):
        return self._model_type

    def boundary_conditions(self):
        return self._boundary_conditions

    def initial(self):
        return self._initial

    def source(self):
        return self._source
