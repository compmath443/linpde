class Source:
    """ This class defines point source. 
    In our model point source is a function of time which is added to a certain point 
    after each step.

        Attributes:
            _x (float): x coordinate of point source 
            _y (float): y coordinate of point source
            _impulse (function of time and args): form of impulse
    """

    __slots__ = ["_x", "_y", "_impulse"]

    def __init__(self, x, y, impulse):
        self._x = x
        self._y = y
        self._impulse = impulse

    def x(self):
        return self._x

    def y(self):
        return self._y

    def impulse(self, time, *args):
        return self.impulse(time, *args)
