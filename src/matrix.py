import numpy as np
from numpy import linalg


class Matrix:
    """ This class defines matrices A and B for equation
            u_t + A * u_x + B * u_y = 0.
    They were found after composing system of conservations laws and 
    rewriting it into vectorized form.

    Attributes:
        _density (function (x, y)): density of medium in any point of mesh
        _lame_lambda (function (x, y)): lambda Lame coefficient in any point of mesh
        _lame_mu (function (x, y)): mu Lame coefficient in any point of mesh

    Notes:
        Matrices are brought using methods "matrix_a_seismic", "matrix_b_seismic", 
                                           "matrix_a_acoustic", "matrix_b_acoustic".
    """

    __slots__ = ["_density", "_lame_lambda", "_lame_mu"]

    def __init__(self, density, lame_lambda, lame_mu):
        self._density = density
        self._lame_lambda = lame_lambda
        self._lame_mu = lame_mu

    def density(self, x, y):
        """Gets point (x, y) in defined medium and returns density in it"""
        return self._density(x, y)

    def lame_lambda(self, x, y):
        """Gets point (x, y) in defined medium and returns lambda coefficient in it"""
        return self._lame_lambda(x, y)

    def lame_mu(self, x, y):
        """Gets point (x, y) in defined medium and returns mu coefficient in it"""
        return self._lame_mu(x, y)

    def matrix_a_seismic(self, x, y):
        """Returns matrix A for seismic case in given point"""
        return np.array([[0.0, 0.0, 0.0, -1 * (self.lame_lambda(x, y) + 2 * self.lame_mu(x, y)), 0.0],
                         [0.0, 0.0, 0.0, -1 * self.lame_lambda(x, y), 0.0],
                         [0.0, 0.0, 0.0, 0.0, -1 * self.lame_mu(x, y)],
                         [-1 / self.density(x, y), 0.0, 0.0, 0.0, 0.0],
                         [0.0, 0.0, -1 / self.density(x, y), 0.0, 0.0]])

    def matrix_b_seismic(self, x, y):
        """Returns matrix B for seismic case in given point"""
        return np.array([[0.0, 0.0, 0.0, 0.0, -1 * (self.lame_lambda(x, y) + 2 * self.lame_mu(x, y))],
                         [0.0, 0.0, 0.0, 0.0, -1 * self.lame_lambda(x, y)],
                         [0.0, 0.0, 0.0, -1 * self.lame_mu(x, y), 0.0],
                         [0.0, 0.0, -1 / self.density(x, y), 0.0, 0.0],
                         [0.0, -1 / self.density(x, y), 0.0, 0.0, 0.0]])

    def matrix_a_acoustic(self, x, y):
        """Returns matrix A for acoustic case in given point"""
        return np.array([[0.0, self.lame_lambda(x, y) + 2 / 3 * self.lame_mu(x, y), 0.0],
                         [1 / self.density(x, y), 0.0, 0.0],
                         [0.0, 0.0, 0.0]])

    def matrix_b_acoustic(self, x, y):
        """Returns matrix A for seismic case in given point"""
        return np.array([[0.0, 0.0, self.lame_lambda(x, y) + 2 / 3 * self.lame_mu(x, y)],
                         [0.0, 0.0, 0.0],
                         [1 / self.density(x, y), 0.0, 0.0]])

    def eigenvalues_a_seismic(self, x, y):
        return 0

    def eigenvalues_b_seismic(self, x, y):
        return 0

    def eigenvalues_a_acoustic(self, x, y):
        c = np.sqrt((self.lame_lambda(x, y) + 2 / 3 * self.lame_mu(x, y)) / self.density(x, y))
        return [-1 * c, c, 0.0]

    def eigenvalues_b_acoustic(self, x, y):
        return 0

    @staticmethod
    def eigenvalues(matrix):
        return linalg.eig(matrix)[0]

    @staticmethod
    def eigenvectors(matrix):
        return linalg.eig(matrix)[1]

    @staticmethod
    def check_product(matrix, eps=10 ** (-4)):
        lambda_matrix = np.diag(Matrix.eigenvalues(matrix))
        left_eigen_matrix = Matrix.eigenvectors(matrix)
        right_eigen_matrix = linalg.inv(left_eigen_matrix)  # it is simply an inverse matrix
        return linalg.norm(np.dot(np.dot(left_eigen_matrix, lambda_matrix), right_eigen_matrix) - matrix, 1) < eps
