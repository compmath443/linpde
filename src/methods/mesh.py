import numpy as np


class Mesh2D:
    """ This class defines 2-dimensional mesh with given borders and number of points on each axis.
     
    Attributes:
        _x_start (double): coordinate of left border
        _x_end (double): coordinate of right border
        _y_start (double): coordinate of down border
        _y_end (double): coordinate of upper border
        _x_num_points (int): number of points on x axis
        _y_num_points (int): number of points on y axis
        _x_axis (:obj: numpy array): array of points on x axis 
        _y_axis (:obj: numpy array): array of points on y axis
    
    Note: 
        This class uses NumPy functions: linspace, meshgrid. 

    TODO:
        * Check if anything more is needed
    """

    __slots__ = ["_x_start", "_x_end", "_y_start", "_y_end", "_x_num_points", "_y_num_points", "_x_axis", "_y_axis"]

    def __init__(self, x_start, x_end, y_start, y_end, x_num_points=100, y_num_points=100):
        """ Constructor gets borders of medium and numbers of points on each axis and
        sets to attributes 
        """

        self._x_start, self._x_end, self._x_num_points = x_start, x_end, x_num_points
        self._y_start, self._y_end, self._y_num_points = y_start, y_end, y_num_points
        x = np.linspace(x_start, x_end, x_num_points)
        y = np.linspace(y_start, y_end, y_num_points)
        self._x_axis, self._y_axis = np.meshgrid(x, y, sparse=True)

    def x_axis(self):
        """ Getter function for _x_axis"""
        return self._x_axis

    def y_axis(self):
        """ Getter function for _y_axis"""
        return self._y_axis
