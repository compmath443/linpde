def upwind(u, tau, h):
    sigma = tau / h
    return sigma * u[:-1] + (1 - sigma) * u[1:]

upwind.name = "upwind"
