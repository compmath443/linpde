from src.methods.bicompact import *
from src.methods.central import *
from src.methods.fedorenko import *
from src.methods.lax import *
from src.methods.mccormaсk import *
from src.methods.tvd import *
from src.methods.upwind import *


class Method:
    def __init__(self, mesh):
        self._mesh = mesh

    def mesh(self):
        return self._mesh

    @staticmethod
    def solve_advection(u_initial, boundary_condition, c, method_name,
                        time_max=0.6, number_of_points=200, courant=0.5, verbose=False):
        """
        if method_name == "bicompact":
            method = bicompact
        elif method_name == "central":
            method = central
        elif method_name == "fedorenko":
            method = fedorenko
        elif method_name == "lax":
            method = lax
        elif method_name == "mccormack":
            method = mccormack
        elif method_name == "tvd":
            method = tvd
        else:
        """

        method = upwind
        print(method.name)
        axis = np.linspace(0, 1, number_of_points + 1)
        axis_step = axis[1] - axis[0]
        if verbose:
            print('Method: ', method.name, ', Courant number: c * tau / h = ', courant, sep='')
        tau = courant * axis_step / c
        u = u_initial(axis)
        time, steps = 0.0, 0
        while time < time_max:
            if time + tau > time_max:
                tau = time_max - time + 1e-14
            u_next = np.empty_like(u)
            u_next[0] = boundary_condition(time + tau)
            u_next[1:] = method(u, tau, axis_step)  # In other points u is calculated by given scheme
            u = u_next                              # Moving to the next time-layer
            time += tau
            steps += 1
        if verbose:
            print('time = ', time, ', made ', steps, ' steps', sep='')
        return axis, u
