import numpy as np


def central(u, tau, h):
    sigma = tau / h
    return np.append([u[1:-1] - sigma / 2 * (u[2:] - u[:-2])],
                     [sigma * u[-2] + (1 - sigma) * u[-1]])

central.name = "central"
