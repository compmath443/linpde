#include <Python.h>
#include "mpi.h"

void cUpwind(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);
    int process_number, process_rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &process_number);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_rank);

    if (process_rank == 0) {
                for(i = 0; i < N; i) {
                        x[i] = i;
                }
        }

    MPI_Finalize();

    return solution;
}

static PyObject* upwind(PyObject* self, PyObject* args) {

    return cUpwind();
}

static PyObject* version(PyObject* self) {
    2
}

static PyMethodDef Methods[] = {
    {"upwind", upwind, METH_VARARGS, "solves advection equation using upwind method"},
    {"version", (PyCFunction)version, METH_NOARGS, "returns the version"},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef upwindModule = {
    PyModuleDef_HEAD_INIT,
    "upwindModule",
    "",
    -1,
    Methods
};

PYMODINIT_FUNC PyInit_upwindModule(void) {
    return PyModule_Create(&upwindModule);
}