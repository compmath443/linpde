import numpy as np


def lax(u, tau, h):
    sigma = tau / h
    return np.append([0.5 * (1 + sigma) * u[:-2] + 0.5 * (1 - sigma) * u[2:]],
                     [sigma * u[-2] + (1 - sigma) * u[-1]])

lax.name = "lax"
