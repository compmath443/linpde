class BoundaryConditions:

    __slots__ = ["_rect", "_left", "_right", "_down", "_up"]

    def __init__(self, left, right, bottom, up, b1, b2, f1, b3, b4, f2, b5, b6, f3, b7, b8, f4):
        self._rect = [left, right, bottom, up]
        self._left = [b1, b2, f1]
        self._right = [b3, b4, f2]
        self._down = [b5, b6, f3]
        self._up = [b7, b8, f4]

    def rect(self):
        return self._rect

    def left(self):
        return self._left

    def right(self):
        return self._right

    def down(self):
        return self._down

    def up(self):
        return self._up
