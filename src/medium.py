class Medium:
    """ This class defines medium for given problem by setting 
    its density and Lame coefficients as functions of coordinates. Using this parameters 
    Youngs modulus and Poissons ration can be calculated in any given point of medium.
    This class is used to define matrices A and B for initial equation.
    
    Attributes:
        _density (function (x, y)): density of medium in any point of mesh
        _lame_lambda (function (x, y)): lambda Lame coefficient in any point of mesh
        _lame_mu (function (x, y)): mu Lame coefficient in any point of mesh
    
    Notes:
        Mesh is highly advised to be created as an instance of class Mesh2D (defined in <mesh.py>)
        
    TODO:
        * Check if anything more is needed
    """

    __slots__ = ["_density", "_lame_lambda", "_lame_mu"]

    def __init__(self, density, lame_lambda, lame_mu):
        self._density = density
        self._lame_lambda = lame_lambda
        self._lame_mu = lame_mu

    def density(self, x, y):
        """Gets point (x, y) in defined medium and returns density in it"""
        return self._density(x, y)

    def lame_lambda(self, x, y):
        """Gets point (x, y) in defined medium and returns lambda coefficient in it"""
        return self._lame_lambda(x, y)

    def lame_mu(self, x, y):
        """Gets point (x, y) in defined medium and returns mu coefficient in it"""
        return self._lame_mu(x, y)

    def youngs_modulus(self, x, y):
        """Gets point (x, y) in defined medium and returns Youngs modulus in it"""
        return self._lame_mu(x, y) * (3 * self._lame_lambda(x, y) +
                                      2 * self._lame_mu(x, y)) / (self._lame_lambda(x, y) +
                                                                  self._lame_mu(x, y))

    def poisson_ratio(self, x, y):
        """Gets point (x, y) in defined medium and returns Poissons ratio in it"""
        return self._lame_lambda(x, y) / (self._lame_lambda(x, y) + self._lame_mu(x, y)) / 2
