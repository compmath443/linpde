import numpy as np


def sinus_source(time, omega, amplitude):
    return amplitude * np.sin(omega * time)


def peak_source(time):
    # second derivative of normal distribution
    return time


def step(time, start, end, height):
    if start <= time <= end:
        return height
    else:
        return 0
