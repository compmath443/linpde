from src.solver import *
from src.visualisation import *


def density(x, y):
    return 50 - (x - 5.0) ** 2 - (y - 5.0) ** 2


def lame_lambda(x, y):
    return 1


def lame_mu(x, y):
    return 1


def impulse(time, omega=2.0, amplitude=10.0):
    return amplitude * np.sin(omega * time)

my_solver = Solver(model_type="Acoustic", x_start=0.0, x_end=10.0, y_start=0.0, y_end=10.0,
                   x_num_points=200, y_num_points=200,
                   time_max=100, time_steps=1,
                   method_name="upwind",
                   density=density, lame_lambda=lame_lambda, lame_mu=lame_mu,
                   u_initial=0.0,
                   b1=1, b2=2, f1=1,
                   b3=1, b4=2, f2=1,
                   b5=1, b6=2, f3=1,
                   b7=1, b8=2, f4=1)

my_solver.model().add_point_source(x=5.0, y=5.0, impulse=impulse)

# solution = my_solver.solve()

matrix_heatmap(my_solver.model().mesh().x_axis(),
               my_solver.model().mesh().y_axis(),
               my_solver.model().medium().density)


