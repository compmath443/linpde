from src.methods.method import *
from src.model import *
from src.matrix import *


class Solver:
    """ This is the main class. It gets all needed parameters and creates a mesh,
    a medium, matrices A and B, splits problem into two one dimensional, 
    imports a method and runs it consequently on these problems.
    
    Attributes:
        _model ():
        _method ():
        _time_scale ():
      
    TODO:
        * finish documentation
        * check test cases
    
    """

    __slots__ = ["_model", "_method", "_time_scale"]

    def __init__(self, model_type, x_start, x_end, y_start, y_end, x_num_points, y_num_points, time_max, time_steps,
                 method_name, density, lame_lambda, lame_mu, u_initial, b1, b2, f1, b3, b4, f2, b5, b6, f3, b7, b8, f4):
        """
        Parameters: type (str): type of problem 
                        ["Seismic", "Acoustic"]
                    x_start (double): coordinate of left border
                    x_end (double): coordinate of right border
                    y_start (double): coordinate of down border
                    y_end (double): coordinate of upper border
                    x_num_points (int): number of points on x axis
                    y_num_points (int): number of points on y axis
                    method_name (str): name of method to be used to solve transport equation
                        ["Upwind", "Fedorenko", "McCormack", "TVD", "Bicompact"]
                    density (function (x, y)): density of medium in any point of mesh
                    lame_lambda (function (x, y)): lambda Lame coefficient in any point of mesh
                    lame_mu (function (x, y)): mu Lame coefficient in any point of mesh 
        """

        self._model = Model(model_type, x_start, x_end, y_start, y_end, x_num_points, y_num_points,
                            density, lame_lambda, lame_mu, u_initial, b1, b2, f1, b3, b4, f2, b5, b6, f3, b7, b8, f4)
        self._method = method_name
        self._time_scale = np.linspace(0.0, time_max, time_steps)

    def model(self):
        return self._model

    def method(self):
        return self._method

    def time_scale(self):
        return self._time_scale

    @staticmethod
    def solve_advection_system(u_initial, boundary_condition, method_name, time_max, matrix):

        left_eigen_matrix = Matrix.eigenvectors(matrix)
        right_eigen_matrix = linalg.inv(left_eigen_matrix)
        eigenvalues = Matrix.eigenvalues(matrix)

        solution_riemann_vars = []
        for eigenvalue in eigenvalues:
            solution_riemann_vars.append(Method.solve_advection(u_initial=u_initial,
                                                                boundary_condition=boundary_condition,
                                                                c=eigenvalue,
                                                                method_name=method_name,
                                                                time_max=time_max,
                                                                number_of_points=200, courant=0.5, verbose=False)[1])
        solution_riemann_vars = np.ndarray(solution_riemann_vars)
        return np.dot(right_eigen_matrix, solution_riemann_vars)

    def solve(self):

        current_axis = 'x'
        solution = {self._time_scale[0]: self.model().initial()}
        time_step = self._time_scale[1] - self._time_scale[0]

        for time in self._time_scale[1::]:

            if current_axis == 'x':
                x_advection_solution_t = \
                    Solver.solve_advection_system(u_initial=self.model().initial(),
                                                  boundary_condition=self.model().boundary_conditions(),
                                                  method_name=self.method(),
                                                  time_max=time_step,
                                                  matrix=self.model().matrix_a())

                y_advection_solution_t_next = \
                    Solver.solve_advection_system(u_initial=x_advection_solution_t,
                                                  boundary_condition=self.model().boundary_conditions(),
                                                  method_name=self.method(),
                                                  time_max=time_step,
                                                  matrix=self.model().matrix_b())

                solution[time] = y_advection_solution_t_next
                current_axis = 'y'

            elif current_axis == 'y':
                y_advection_solution_t = \
                    Solver.solve_advection_system(u_initial=self.model().initial(),
                                                  boundary_condition=self.model().boundary_conditions(),
                                                  method_name=self.method(),
                                                  time_max=time_step,
                                                  matrix=self.model().matrix_b())

                x_advection_solution_t_next = \
                    Solver.solve_advection_system(u_initial=y_advection_solution_t,
                                                  boundary_condition=self.model().boundary_conditions(),
                                                  method_name=self.method(),
                                                  time_max=time_step,
                                                  matrix=self.model().matrix_a())

                solution[time] = x_advection_solution_t_next
                current_axis = 'x'

        return solution
